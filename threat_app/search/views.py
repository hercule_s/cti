from django.shortcuts import render

from search.documents import AttackerDocument, OperationDocument, TTPDocument, TargetDocument

def search(request):

    q = request.GET.get('q')

    if q:
        intel = AttackerDocument.search().query("match", name=q)
    else:
        intel = OperationDocument.search().query("match", name=q)

    return render(request, 'intel/search.html', {'intel': intel})
