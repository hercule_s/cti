from django_elasticsearch_dsl import DocType, Index
from intel.models import Attacker, Operation, TTP, Target

intel = Index('intel')

@intel.doc_type
class AttackerDocument(DocType):
    class Meta:
        model = Attacker

        fields = [
            'name',
            'attacker_category',
        ]

@intel.doc_type
class OperationDocument(DocType):
    class Meta:
        model = Operation

        fields = [
            'name',
            'motivation',
        ]

@intel.doc_type
class TTPDocument(DocType):
    class Meta:
        model = TTP

        fields = [
            'name',
        ]

@intel.doc_type
class TargetDocument(DocType):
    class Meta:
        model = Target

        fields = [
            'name',
            'country',
        ]