from django.db import models

class Attacker(models.Model):
    name = models.CharField(max_length=200)
    attacker_category = models.CharField(max_length=200)
    
    def save(self):
        super(Attacker, self).save()

    def __str__(self):
        return self.name

class Target(models.Model):
    name = models.CharField(max_length=200)
    country = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class TTP(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Operation(models.Model):
    name = models.CharField(max_length=200)
    motivation = models.CharField(max_length=200)

    def __str__(self):
        return self.name