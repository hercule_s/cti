from django.http import HttpResponse
from django.shortcuts import render



def index(request):
    return render(request, 'intel/intel.html')

def map(request):
    return render(request, 'intel/map.html')

def assets(request):
    return render(request, 'intel/assets.html')