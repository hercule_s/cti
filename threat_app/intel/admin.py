from django.contrib import admin

from .models import Attacker, Target, Operation, TTP

admin.site.register(Attacker)
admin.site.register(Operation)
admin.site.register(Target)
admin.site.register(TTP)